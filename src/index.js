import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import "bootstrap/dist/css/bootstrap.min.css";
import configureStore from "./store/configureStore";
import { PersistGate } from "redux-persist/lib/integration/react";
import "./index.css";
import App from "./containers/App";
import registerServiceWorker from "./registerServiceWorker";

const { store, persistor } = configureStore();
ReactDOM.render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <App />
        </PersistGate>
    </Provider>,
    document.getElementById("root")
);
registerServiceWorker();
