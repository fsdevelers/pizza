import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as appActions from "../actions/appActions";
import Graph from "../components/Graph";
import { Container, Row, Col, Button } from "reactstrap";
import Header from "../components/Header";
import Error from "../components/Error";
import Register from "../components/Register";
import "./App.css";
class App extends Component {
    componentWillMount() {
        this.props.appActions.fetchVotes();
    }
    render() {
        const { token, user, show } = this.props;
        return (
            <div className="App">
                <Header {...this.props} />
                {"   "}
                <Register {...this.props} />
                <Error {...this.props} />
                <Container>
                    <Row>
                        <Col sm={{ size: 10, offset: 1 }}>
                            <Graph {...this.props} />
                        </Col>
                    </Row>
                    {token === null && (
                        <Row>
                            <Col sm={{ size: 10, offset: 1 }}>
                                <Button
                                    size="lg"
                                    onClick={() => {
                                        this.props.appActions.setShow(!show);
                                    }}
                                    color="danger"
                                >
                                    Register
                                </Button>{" "}
                            </Col>
                        </Row>
                    )}
                    {token !== null && (
                        <Row>
                            <Col sm={{ size: 10, offset: 1 }}>
                                <Button
                                    size="lg"
                                    onClick={() => {
                                        this.props.appActions.vote(
                                            token,
                                            user._id
                                        );
                                    }}
                                    color="warning"
                                >
                                    I love it!
                                </Button>{" "}
                            </Col>
                        </Row>
                    )}
                </Container>
            </div>
        );
    }
}
function mapStateToProps(state) {
    return {
        votes: state.appRed.votes,
        width: state.appRed.width,
        error: state.appRed.error,
        show: state.appRed.show,
        token: state.appRed.token,
        user: state.appRed.user,
        register: state.appRed.register
    };
}

function mapDispatchToProps(dispatch) {
    return {
        appActions: bindActionCreators(appActions, dispatch)
    };
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
