import React from "react";
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink
} from "reactstrap";
import Login from "./Login";
class Header extends React.Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    render() {
        return (
            <div>
                <Navbar color="light" light expand="md">
                    <NavbarBrand href="/">Pizza!</NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem className="mr-4">
                                {this.props.user !== null && (
                                    <p className="text-muted mt-2">
                                        Welcome{" "}
                                        <strong className="text-capitalize">
                                            {this.props.user.name}
                                        </strong>{" "}
                                        Email:{" "}
                                        <strong>{this.props.user.email}</strong>{" "}
                                    </p>
                                )}{" "}
                            </NavItem>
                            <NavItem className="ml-auto">
                                <Login {...this.props} />
                            </NavItem>
                            <NavItem>
                                <NavLink
                                    target="_blank"
                                    href="https://bitbucket.com/aalexx1978"
                                >
                                    More Apps
                                </NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        );
    }
}
export default Header;
