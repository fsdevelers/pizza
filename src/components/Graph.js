import React, { Component } from "react";
import Divsize from "./Divsize";
import {
    AreaChart,
    Area,
    XAxis,
    YAxis,
    Tooltip,
    CartesianGrid,
    Legend
} from "recharts";
import { Jumbotron } from "reactstrap";
class Graph extends Component {
    render() {
        const data1 = this.props.votes;
        if (data1.length > 0 || this.props.width !== null) {
            return (
                <div>
                    <Jumbotron>
                        {" "}
                        <Divsize {...this.props} />
                        <h3 className="display-3">Top voters!</h3>
                        <AreaChart
                            width={this.props.width}
                            height={200}
                            data={data1}
                            syncId="City"
                            margin={{ top: 10, right: 30, left: 0, bottom: 0 }}
                        >
                            <defs>
                                <linearGradient
                                    id="colorUv"
                                    x1="0"
                                    y1="0"
                                    x2="0"
                                    y2="1"
                                >
                                    <stop
                                        offset="40%"
                                        stopColor="#8884d8"
                                        stopOpacity={0.8}
                                    />
                                    <stop
                                        offset="60%"
                                        stopColor="#FFAE19"
                                        stopOpacity={0.8}
                                    />
                                </linearGradient>
                            </defs>
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="name" tick={{ fill: "white" }} />
                            <YAxis tick={{ fill: "white" }} />
                            <Tooltip
                                cursor={false}
                                itemStyle={{ color: "black" }}
                                wrapperStyle={{
                                    color: "black",
                                    backgroundColor: "red"
                                }}
                            />
                            <Legend verticalAlign="top" height={36} />
                            <Area
                                type="monotone"
                                dataKey="votes"
                                stroke="#8884d8"
                                fillOpacity={1}
                                fill="url(#colorUv)"
                            />
                        </AreaChart>
                    </Jumbotron>
                </div>
            );
        } else {
            return <Divsize {...this.props} />;
        }
    }
}
export default Graph;
