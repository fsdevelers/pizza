import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import RegisterForm from "./RegisterForm";
class Register extends React.Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.props.appActions.setShow(!this.props.show);
    }

    render() {
        return (
            <div>
                <Modal
                    isOpen={this.props.show}
                    toggle={this.toggle}
                    className={this.props.className}
                >
                    <ModalHeader toggle={this.toggle}>
                        Register and vote!
                    </ModalHeader>
                    <ModalBody>
                        <RegisterForm {...this.props} />
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggle}>
                            Cancel
                        </Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default Register;
